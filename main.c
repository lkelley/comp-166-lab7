#include <stdio.h>
#include <stdlib.h>

typedef int WORD;
#define WORD_FORMAT "%d "
static const WORD START = 1011;
static const int N_WORDS = 10;

// Global to obviate pointer passing
static WORD *wordVector;

void loadArray() __attribute__((always_inline));


/**
 * Swaps x and y in memory
 *
 * @param x
 * @param y
 */
void swap(WORD *x, WORD *y) {
    // Store the WORD that x points to in temp
    WORD temp = *x;

    // Store the WORD pointed to by y at the address pointed to by x
    *x = *y;

    // Store the temp value at the address pointed to by y
    *y = temp;
}


/**
 * Prints each value in wordVector
 * @param vector
 * @param N
 */
void printVector(const WORD *vector, const int N) {
    /*
     * Iterate over the vector, printing each value
     */
    for (int i = 0; i < N; i++) {
        printf(WORD_FORMAT, vector[i]);
    }

    puts("\n");
}


/**
 * Prints all values from wordVector, byte by byte
 * @param vector
 * @param N
 */
void printBytes(const WORD *vector, const int N) {
    unsigned char *bytes = (unsigned char *)vector;
    for (int i = 0; i < sizeof(WORD) * N; i++) {
        printf("%i ", bytes[i]);
    }

    puts("\n");
}


/**
 * Prints all values from wordVector, byte by byte in reverse endian
 * @param vector
 * @param N
 */
void printVectorReverseEndian(const WORD *vector, const int N) {
    // Iterate over vector
    for (int i = 0; i < N; i++) {
        // Get current WORD
        unsigned char *bytes = (unsigned char *)&vector[i];

        // Iterate over bytes in WORD
        for (long j = (long)sizeof(WORD) - 1; j >= 0; j--) {
            printf("%i ", bytes[j]);
        }
    }

    puts("\n");
}


/**
 * Loads the array full of the specified count of numbers
 */
void loadArray() {
    wordVector = (WORD *)malloc(sizeof(WORD) * N_WORDS);

    for (WORD i = 0; i < N_WORDS; i++) {
        wordVector[i] = START + i;
    }
}


/**
 * Swaps adjacent words in the vector
 */
void swapAdjacents() {
    for (int i = 0; i < N_WORDS; i += 2) {
        // Check that i+1 is not out of bounds
        if (i + 1 >= N_WORDS) continue;

        swap(&wordVector[i], &wordVector[i + 1]);
    }
}


int main() {
#pragma clang diagnostic push
#pragma ide diagnostic ignored "OCSimplifyInspection"
    if (N_WORDS % 2 != 0) {
        puts("Warning: Odd number of words entered. Expect the unexpected.");
    }
#pragma clang diagnostic pop

    loadArray();

    puts("Original Vector:\n");
    printVector(wordVector, N_WORDS);

    swapAdjacents();

    puts("Adjacent words swapped:\n");
    printVector(wordVector, N_WORDS);

    swapAdjacents();

    puts("Words swapped back:\n");
    printVector(wordVector, N_WORDS);

    puts("Data Bytes:\n");
    printBytes(wordVector, N_WORDS);

    puts("Word bytes with endian reversal:\n");
    printVectorReverseEndian(wordVector, N_WORDS);

    return EXIT_SUCCESS;
}
